package com.nixsolutions.dev.task.controller;


import com.nixsolutions.dev.task.dto.ProductDto;
import com.nixsolutions.dev.task.services.ProductService;
import com.nixsolutions.dev.task.util.ResponseFactory;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("api/rest/products")
@ApiResponses(value = {
        @ApiResponse(code = 500, message = "Server Error"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
public class ProductController {


    private final ProductService productService;
    private final ResponseFactory<ProductDto> responseProductFactory;

    @Autowired
    public ProductController(ProductService productService, ResponseFactory<ProductDto> productFactory) {
        this.productService = productService;
        this.responseProductFactory = productFactory;
    }

    @GetMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved product by id")
    public ResponseEntity<ProductDto> getById(@PathVariable(name = "id") String id) {
        ProductDto product = productService.findById(id);
        return responseProductFactory.createResponse(product);
    }


    @GetMapping(value = "/byName/{name}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved product by name")
    public ResponseEntity<List<ProductDto>> getByName(@PathVariable(name = "name") String name) {
        List<ProductDto> productList = productService.findByName(name);
        return responseProductFactory.createResponse(productList);
    }


    @GetMapping(value = "/start/{start}/finish/{finish}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved product between start and finish")
    public ResponseEntity<List<ProductDto>> getBetween(@PathVariable(name = "start") Double start,
                                                       @PathVariable(name = "finish") Double finish) {
        List<ProductDto> product = productService.findByPriceBetween(start, finish);
        return responseProductFactory.createResponse(product);

    }


    @GetMapping(value = "/byPrice/{price}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved product by price")
    public ResponseEntity<List<ProductDto>> getByPrice(@PathVariable(name = "price") Double price) {
        List<ProductDto> productList = productService.findByPrice(price);
        return responseProductFactory.createResponse(productList);
    }


    @PostMapping(value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 201, message = "Success")
    public ResponseEntity<ProductDto> save(@RequestBody @Valid ProductDto product) {
        ProductDto returnedProduct = productService.save(product);
        return ResponseEntity.status(201).body(returnedProduct);
    }

    @DeleteMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 204, message = "Product successfully removed")
    public ResponseEntity<ProductDto> remove(@PathVariable(name = "id") String id) {
        productService.remove(id);
        return ResponseEntity.status(204).build();
    }
}