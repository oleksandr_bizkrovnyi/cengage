package com.nixsolutions.dev.task.controller;

import com.nixsolutions.dev.task.dto.AddressDto;
import com.nixsolutions.dev.task.dto.ProductCount;
import com.nixsolutions.dev.task.services.AddressService;
import com.nixsolutions.dev.task.util.ResponseFactory;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/rest/address")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 500, message = "Server Error"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
public class AddressController {


    private final AddressService addressService;
    private final ResponseFactory<AddressDto> responseAddressFactory;


    @Autowired
    public AddressController(AddressService addressService, ResponseFactory<AddressDto> addressFactory) {
        this.addressService = addressService;
        this.responseAddressFactory = addressFactory;
    }

    @GetMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved by id")
    public ResponseEntity<AddressDto> getById(@PathVariable(name = "id") String id) {
        AddressDto address = addressService.findById(id);
        return responseAddressFactory.createResponse(address);
    }


    @GetMapping(value = "/byCity/{city}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved by city")
    public ResponseEntity<List<AddressDto>> getAddressesByCity(@PathVariable(name = "city") String city) {
        List<AddressDto> addressDtoList = addressService.findByCity(city);
        return responseAddressFactory.createResponse(addressDtoList);
    }


    @GetMapping(value = "/product/count",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Product Count successfully retrieved")
    public ResponseEntity<List<ProductCount>> getProductCount() {
        List<ProductCount> productCount = addressService.getProductCount();
        return ResponseEntity.ok(productCount);
    }

    @GetMapping(value = "/byStreet/{street}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "  Addresses List successfully retrieved")
    public ResponseEntity<List<AddressDto>> getAddressesByStreet(@NotNull @PathVariable(name = "street") String street) {
        List<AddressDto> addressDtoList = addressService.findByStreet(street);
        return responseAddressFactory.createResponse(addressDtoList);
    }


    @GetMapping(value = "byCity/{city}/AndStreet/{street}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Address successfully retrieved")
    public ResponseEntity<List<AddressDto>> getByCityAndStreet(@PathVariable(name = "city") String city,
                                                               @PathVariable(name = "street") String street) {
        List<AddressDto> addressDto = addressService.findByCityAndStreet(city, street);
        return responseAddressFactory.createResponse(addressDto);
    }


    @PostMapping(value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 201, message = "Success")
    public ResponseEntity<AddressDto> save(@RequestBody @Valid AddressDto address) {
        AddressDto savedAddress = addressService.save(address);
        return responseAddressFactory.createResponse(savedAddress);
    }


    @DeleteMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 204, message = "Address successfully removed")
    public ResponseEntity remove(@PathVariable(name = "id") String id) {
        addressService.remove(id);
        return ResponseEntity.status(201).build();
    }

}
