package com.nixsolutions.dev.task.controller;


import com.nixsolutions.dev.task.dto.ProducerDto;
import com.nixsolutions.dev.task.services.ProducerService;
import com.nixsolutions.dev.task.util.ResponseFactory;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("api/rest/producers")
@ApiResponses(value = {
        @ApiResponse(code = 500, message = "Server Error"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
public class ProducerController {


    private final ProducerService producerService;
    private final ResponseFactory<ProducerDto> responseProducerFactory;


    @Autowired
    public ProducerController(ProducerService producerService, ResponseFactory<ProducerDto> producerFactory) {
        this.producerService = producerService;
        this.responseProducerFactory = producerFactory;
    }

    @GetMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved producer by id")
    public ResponseEntity<ProducerDto> getById(@PathVariable(name = "id") String id) {
        ProducerDto producer = producerService.findById(id);
        return responseProducerFactory.createResponse(producer);
    }


    @GetMapping(value = "/byName/{name}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved producer by name")
    public ResponseEntity<List<ProducerDto>> getByName(@PathVariable(name = "name") String name) {
        List<ProducerDto> producer = producerService.findByName(name);
        return responseProducerFactory.createResponse(producer);
    }


    @PostMapping(value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 201, message = "Success")
    public ResponseEntity<ProducerDto> save(@RequestBody @Valid ProducerDto producer) {
        ProducerDto createdProducer = producerService.save(producer);
        return ResponseEntity.status(201).body(createdProducer);
    }


    @DeleteMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 204, message = "Producer successfully removed")
    public ResponseEntity remove(@PathVariable(name = "id") String id) {
        producerService.remove(id);
        return ResponseEntity.status(204).build();
    }
}
