package com.nixsolutions.dev.task.controller;

import com.nixsolutions.dev.task.dto.CustomerDto;
import com.nixsolutions.dev.task.services.CustomerService;
import com.nixsolutions.dev.task.util.ResponseFactory;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/rest/customers")
@ApiResponses(value = {
        @ApiResponse(code = 500, message = "Server Error"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")})
public class CustomerController {

    private final CustomerService customerService;
    private final ResponseFactory<CustomerDto> responseCustomerFactory;

    @Autowired
    public CustomerController(CustomerService customerService, ResponseFactory<CustomerDto> customerFactory) {
        this.customerService = customerService;
        this.responseCustomerFactory = customerFactory;
    }

    @GetMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved customer by id")
    public ResponseEntity<CustomerDto> getById(@PathVariable(name = "id") String id) {
        CustomerDto customer = customerService.findById(id);
        return responseCustomerFactory.createResponse(customer);
    }


    @GetMapping(value = "/byName/{name}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 200, message = "Successfully retrieved customer by name")
    public ResponseEntity<List<CustomerDto>> getCustomersByName(@PathVariable(name = "name") String name) {
        List<CustomerDto> customerList = customerService.findByName(name);
        return responseCustomerFactory.createResponse(customerList);
    }


    @PostMapping(value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 201, message = "Success")
    public ResponseEntity<CustomerDto> save(@RequestBody @Valid CustomerDto customer) {
        CustomerDto createdCustomer = customerService.save(customer);
        return ResponseEntity.status(201).body(createdCustomer);
    }


    @DeleteMapping(value = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ApiResponse(code = 204, message = "Customer successfully removed")
    public ResponseEntity remove(@PathVariable(name = "id") String id) {
        customerService.remove(id);
        return ResponseEntity.status(204).build();

    }
}
