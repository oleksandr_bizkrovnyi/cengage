package com.nixsolutions.dev.task.services.impl;

import com.nixsolutions.dev.task.domain.Product;
import com.nixsolutions.dev.task.dto.ProductDto;
import com.nixsolutions.dev.task.mapper.ProductMapper;
import com.nixsolutions.dev.task.repositories.ProductRepository;
import com.nixsolutions.dev.task.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;


@Service("productService")
@Transactional
@Slf4j
public class ProductServiceImpl implements ProductService {


    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public ProductDto findById(String id) {
        Optional<Product> foundProduct = productRepository.findById(id);
        log.info("found product");
        return productMapper.toDto(foundProduct.orElse(null));

    }

    @Override
    public List<ProductDto> findByName(String name) {
        List<Product> foundProduct = productRepository.findAllByName(name);
        log.info("found products");
        return foundProduct.stream().map(productMapper::toDto).collect(toList());
    }

    @Override
    public List<ProductDto> findByPriceBetween(double start, double finish) {
        if (finish < start) {
            throw new IllegalArgumentException("Start greater than finish");
        }
        List<Product> foundProduct = productRepository.findAllByPriceBetween(start, finish);
        log.info("found products");
        return foundProduct.stream().map(productMapper::toDto).collect(toList());
    }

    @Override
    public List<ProductDto> findByPrice(double price) {
        List<Product> foundProduct = productRepository.findAllByPrice(price);
        log.info("found product");
        return foundProduct.stream().map(productMapper::toDto).collect(toList());
    }

    @Override
    public ProductDto save(ProductDto product) {
        Product savedProduct = productRepository.save(productMapper.toDomain(product));
        log.info("product created");
        return productMapper.toDto(savedProduct);

    }


    @Override
    public void remove(String id) {
            productRepository.deleteById(id);
            log.info("product removed");

    }

}
