package com.nixsolutions.dev.task.services;

import com.nixsolutions.dev.task.domain.Product;
import com.nixsolutions.dev.task.dto.ProductDto;

import java.util.List;

public interface ProductService {

    ProductDto findById(String id);

    List<ProductDto> findByName(String name);

    List<ProductDto> findByPriceBetween(double start, double finish);

    List<ProductDto> findByPrice(double price);

    ProductDto save(ProductDto product);

    void remove(String id);
}
