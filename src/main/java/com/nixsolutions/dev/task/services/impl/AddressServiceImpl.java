package com.nixsolutions.dev.task.services.impl;


import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.dto.AddressDto;
import com.nixsolutions.dev.task.dto.ProductCount;
import com.nixsolutions.dev.task.mapper.AddressMapper;
import com.nixsolutions.dev.task.mapper.ProductCountMapper;
import com.nixsolutions.dev.task.repositories.AddressRepository;
import com.nixsolutions.dev.task.repositories.queryDsl.DSLProductRepository;
import com.nixsolutions.dev.task.services.AddressService;
import com.querydsl.core.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Slf4j
@Transactional
@Service("addressService")
public class AddressServiceImpl implements AddressService {

    private final AddressRepository repository;
    private final DSLProductRepository dslProductRepository;
    private final ProductCountMapper productCountMapper;
    private final AddressMapper addressMapper;

    @Autowired
    public AddressServiceImpl(AddressRepository repository,
                              DSLProductRepository dslProductRepository,
                              ProductCountMapper productCountMapper, AddressMapper addressMapper) {
        this.repository = repository;
        this.dslProductRepository = dslProductRepository;
        this.productCountMapper = productCountMapper;
        this.addressMapper = addressMapper;
    }

    @Override
    public AddressDto findById(String id) {
        Optional<Address> address = repository.findById(id);
        log.info("Getting Address by id");
        return address.map(addressMapper::toDto).orElse(null);
    }

    @Override
    public List<AddressDto> findByCity(String city) {
        log.info("Getting Address by city");
        List<Address> addresses = repository.findAllByCity(city);
        return addresses.stream().map(addressMapper::toDto).collect(toList());
    }

    @Override
    public List<AddressDto> findByCityAndStreet(String city, String street) {
        log.info("Getting Address by city and street");
        return repository.findAllByCityAndStreet(city, street).stream()
                .map(addressMapper::toDto)
                .collect(toList());
    }

    @Override
    public List<AddressDto> findByStreet(String street) {
        log.info("Getting Address list by street");
        return repository.findAllByStreet(street).stream()
                .map(addressMapper::toDto)
                .collect(toList());
    }

    @Override
    public AddressDto save(AddressDto address) {
        Address savedAddress = repository.save(addressMapper.toDomain(address));
        log.info("Saving Address");
        return addressMapper.toDto(savedAddress);
    }


    @Override
    public void remove(String id) {
        repository.deleteById(id);
        log.info("Deleting Address");
    }

    @Override
    public List<ProductCount> getProductCount() {
        List<Tuple> result = dslProductRepository.getProductsPerAddress();
        log.info("Creating ProductCount list");
        return result.stream()
                .map(productCountMapper::toDto)
                .collect(toList());

    }


}
