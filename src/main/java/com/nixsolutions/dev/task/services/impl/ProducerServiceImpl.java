package com.nixsolutions.dev.task.services.impl;

import com.nixsolutions.dev.task.domain.Producer;
import com.nixsolutions.dev.task.dto.ProducerDto;
import com.nixsolutions.dev.task.dto.ProductDto;
import com.nixsolutions.dev.task.mapper.ProducerMapper;
import com.nixsolutions.dev.task.mapper.ProductMapper;
import com.nixsolutions.dev.task.repositories.ProducerRepository;
import com.nixsolutions.dev.task.services.ProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;


@Slf4j
@Service("producerService")
@Transactional
public class ProducerServiceImpl implements ProducerService {


    private final ProducerRepository producerRepository;
    private final ProducerMapper producerMapper;
    private final ProductMapper productMapper;

    @Autowired
    public ProducerServiceImpl(ProducerRepository producerRepository, ProducerMapper producerMapper,ProductMapper productMapper) {
        this.producerRepository = producerRepository;
        this.producerMapper = producerMapper;
        this.productMapper = productMapper;
    }


    @Override
    public ProducerDto findById(String id) {
        Optional<Producer> foundProducer = producerRepository.findById(id);
        if (foundProducer.isPresent()) {
            log.info("getting customer by Id");
            return producerMapper.toDto(foundProducer.get());
        }
        return null;
    }

    @Override
    public List<ProducerDto> findByName(String name) {
        List<Producer> foundProducer = producerRepository.findAllByName(name);
        log.info("found producer");
        return foundProducer.stream().map(producerMapper::toDto).collect(toList());
    }


    @Override
    public List<ProducerDto> findByProduct(ProductDto product) {
            List<Producer> foundProducerList = producerRepository.findAllByProduct(productMapper.toDomain(product));
            log.info("found producer");
            return foundProducerList.stream().map(producerMapper::toDto).collect(toList());
    }

    @Override
    public ProducerDto save(ProducerDto producer) {
            Producer savedProducer = producerRepository.save(producerMapper.toDomain(producer));
            log.info("producer created");
            return producerMapper.toDto(savedProducer);
    }


    @Override
    public void remove(String id) {
            producerRepository.deleteById(id);
            log.info("producer removed");
    }
}
