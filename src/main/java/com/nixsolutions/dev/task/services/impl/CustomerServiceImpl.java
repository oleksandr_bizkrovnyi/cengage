package com.nixsolutions.dev.task.services.impl;

import com.google.gson.Gson;
import com.nixsolutions.dev.task.domain.Customer;

import com.nixsolutions.dev.task.dto.CustomerDto;
import com.nixsolutions.dev.task.mapper.CustomerMapper;
import com.nixsolutions.dev.task.repositories.CustomerRepository;
import com.nixsolutions.dev.task.services.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


import static java.util.stream.Collectors.toList;


@Service("customerService")
@Transactional
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final JmsTemplate template;
    private final CustomerMapper customerMapper;


    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, JmsTemplate template, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.template = template;
        this.customerMapper = customerMapper;
    }


    @Override
    public CustomerDto findById(String id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isPresent()) {
            log.info("getting customer by Id");
            return customerMapper.toDto(customer.get());
        }
        return null;
    }

    @Override
    public List<CustomerDto> findByName(String name) {
        List<Customer> customer = customerRepository.findAllByName(name);

        log.info("getting customer by Id");
        return customer.stream().map(customerMapper::toDto).collect(toList());


    }

    @Override
    public CustomerDto save(CustomerDto customer) {
        Customer savedCustomer = customerRepository.save(customerMapper.toDomain(customer));
        log.info("Saving Customer");
        template.convertAndSend("CustomerCreatedOutput", new Gson().toJson(savedCustomer));
        log.info("JMS Send");
        return customerMapper.toDto(savedCustomer);
    }


    @Override
    public void remove(String id) {
        customerRepository.deleteById(id);
        log.info("customer deleted");
    }
}
