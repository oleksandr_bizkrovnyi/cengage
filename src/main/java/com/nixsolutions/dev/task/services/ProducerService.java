package com.nixsolutions.dev.task.services;

import com.nixsolutions.dev.task.domain.Producer;
import com.nixsolutions.dev.task.domain.Product;
import com.nixsolutions.dev.task.dto.ProducerDto;
import com.nixsolutions.dev.task.dto.ProductDto;

import java.util.List;

public interface ProducerService {

    ProducerDto findById(String id);

    List<ProducerDto> findByName(String name);

    List<ProducerDto> findByProduct(ProductDto producer);

    ProducerDto save(ProducerDto producer);

    void remove(String id);
}
