package com.nixsolutions.dev.task.services;

import com.nixsolutions.dev.task.domain.Customer;
import com.nixsolutions.dev.task.dto.CustomerDto;

import java.util.List;

public interface CustomerService {


    CustomerDto findById(String id);

    List<CustomerDto> findByName(String name);

    CustomerDto save(CustomerDto customer);

    void remove(String id);
}
