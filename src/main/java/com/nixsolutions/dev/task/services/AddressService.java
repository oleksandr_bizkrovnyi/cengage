package com.nixsolutions.dev.task.services;


import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.dto.AddressDto;
import com.nixsolutions.dev.task.dto.ProductCount;

import java.util.List;


public interface AddressService {
    AddressDto findById(String id);

    List<AddressDto> findByCity(String city);

    List<AddressDto> findByCityAndStreet(String city, String street);

    List<AddressDto> findByStreet(String street);

    AddressDto save(AddressDto address);

    void remove(String id);

    List<ProductCount> getProductCount();
}
