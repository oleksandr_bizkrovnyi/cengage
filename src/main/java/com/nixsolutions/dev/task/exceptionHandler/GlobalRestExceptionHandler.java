package com.nixsolutions.dev.task.exceptionHandler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalRestExceptionHandler {

    @ExceptionHandler(Exception.class)
    private ResponseEntity<Object> handleExceptionInternal(Exception ex) {
        return  ResponseEntity.status(500).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    private ResponseEntity<Object> handleNullPointer(Exception ex) {
        return  ResponseEntity.status(500).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    private ResponseEntity<Object> handleIllegalArgument(Exception ex) {
        return  ResponseEntity.status(400).body(ex.getMessage());
    }


}
