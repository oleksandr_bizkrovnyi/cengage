package com.nixsolutions.dev.task.config.jms;



import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.stereotype.Component;


@EnableJms
@Component
@ConfigurationProperties(prefix = "spring.activemq")
@EnableConfigurationProperties
public class JmsConfig {

}
