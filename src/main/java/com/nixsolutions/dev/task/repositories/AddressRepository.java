package com.nixsolutions.dev.task.repositories;

import com.nixsolutions.dev.task.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AddressRepository extends JpaRepository<Address, String> {

    List<Address> findAllByCity(String city);

    List<Address> findAllByCityAndStreet(String city, String street);

    List<Address> findAllByStreet(String street);

}
