package com.nixsolutions.dev.task.repositories;


import com.nixsolutions.dev.task.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByName(String name);

    List<Product> findAllByPriceBetween(double start, double finish);

    List<Product> findAllByPrice(double price);

}
