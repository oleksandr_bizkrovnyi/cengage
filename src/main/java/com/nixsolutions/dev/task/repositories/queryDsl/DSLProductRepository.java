package com.nixsolutions.dev.task.repositories.queryDsl;


import com.querydsl.core.Tuple;

import java.util.List;


public interface DSLProductRepository {
    List<Tuple> getProductsPerAddress();
}
