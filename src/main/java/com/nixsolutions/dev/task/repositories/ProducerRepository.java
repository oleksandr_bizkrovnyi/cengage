package com.nixsolutions.dev.task.repositories;


import com.nixsolutions.dev.task.domain.Producer;
import com.nixsolutions.dev.task.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, String> {
    List<Producer> findAllByName(String name);

    List<Producer> findAllByProduct(Product product);

}
