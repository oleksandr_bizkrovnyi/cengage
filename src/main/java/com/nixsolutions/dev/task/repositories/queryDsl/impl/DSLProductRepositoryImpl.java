package com.nixsolutions.dev.task.repositories.queryDsl.impl;


import com.nixsolutions.dev.task.domain.QAddress;
import com.nixsolutions.dev.task.domain.QProducer;
import com.nixsolutions.dev.task.repositories.queryDsl.DSLProductRepository;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

import static com.querydsl.core.types.ExpressionUtils.count;

@Repository
public class DSLProductRepositoryImpl implements DSLProductRepository {

    private final EntityManager entityManager;

    @Autowired
    public DSLProductRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Tuple> getProductsPerAddress() {
        QAddress address = QAddress.address;
        QProducer producer = QProducer.producer;
        JPAQuery<Tuple> query = new JPAQuery<>(entityManager);

        return query.select(address, count(producer.product)).from(producer)
                .innerJoin(producer.address,address)
                .groupBy(address).fetch();
    }


}
