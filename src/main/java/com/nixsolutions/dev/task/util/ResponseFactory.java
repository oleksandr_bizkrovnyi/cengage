package com.nixsolutions.dev.task.util;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ResponseFactory<T> {


    public ResponseEntity<T> createResponse(T obj) {
        if (obj != null) {
            return ResponseEntity.ok(obj);
        }
        return ResponseEntity.noContent().build();
    }

    public ResponseEntity<List<T>> createResponse(List<T> list) {
        if (list.size() > 0) {
            return ResponseEntity.ok(list);
        }
        return ResponseEntity.noContent().build();
    }

}
