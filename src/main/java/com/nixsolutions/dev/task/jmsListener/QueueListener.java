package com.nixsolutions.dev.task.jmsListener;


import com.google.gson.Gson;
import com.nixsolutions.dev.task.domain.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
@Slf4j
@ConfigurationProperties(prefix = "queue")
public class QueueListener {


    @JmsListener(destination = "${queue.customer.updated}")
    public void receiveUpdated(final String jsonMessage) {
        log.info("Received updated " + new Gson().fromJson(jsonMessage, Customer.class).getName());
    }

    @JmsListener(destination = "${queue.customer.error}")
    public void receivedError(final String jsonMessage) {
        log.info("Received error " + new Gson().fromJson(jsonMessage, Customer.class).getName());
    }
}
