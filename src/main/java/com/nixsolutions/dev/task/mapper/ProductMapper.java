package com.nixsolutions.dev.task.mapper;

import com.nixsolutions.dev.task.domain.Product;
import com.nixsolutions.dev.task.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mappings({
            @Mapping(source = "productIdentify",target = "id"),
            @Mapping(source = "productName",target = "name"),
            @Mapping(source = "cost",target = "price")
    })
    Product toDomain(ProductDto productDto);


    @Mappings({
            @Mapping(source = "id",target = "productIdentify"),
            @Mapping(source = "name",target = "productName"),
            @Mapping(source = "price",target = "cost")
    })
    ProductDto toDto(Product address);
}
