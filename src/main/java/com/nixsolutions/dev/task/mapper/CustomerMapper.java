package com.nixsolutions.dev.task.mapper;


import com.nixsolutions.dev.task.domain.Customer;
import com.nixsolutions.dev.task.dto.CustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;



@Mapper(componentModel = "spring")
public interface CustomerMapper {

    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "address",target = "address")
    })
    Customer toDomain(CustomerDto customerDto);


    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "address",target = "address")
    })
    CustomerDto toDto(Customer customer);

}
