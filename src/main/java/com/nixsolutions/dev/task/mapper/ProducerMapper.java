package com.nixsolutions.dev.task.mapper;



import com.nixsolutions.dev.task.domain.Producer;
import com.nixsolutions.dev.task.dto.ProducerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface ProducerMapper {

    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "address",target = "address"),
            @Mapping(source = "product",target = "product")
    })
    Producer toDomain(ProducerDto producerDto);


    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "address",target = "address"),
            @Mapping(source = "product",target = "product")
    })
    ProducerDto toDto(Producer producer);
}
