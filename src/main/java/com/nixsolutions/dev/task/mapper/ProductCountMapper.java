package com.nixsolutions.dev.task.mapper;


import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.dto.ProductCount;
import com.querydsl.core.Tuple;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;


@Mapper(componentModel = "spring")
public interface ProductCountMapper {

    @Mappings({
            @Mapping(target = "count", source = "element", qualifiedByName = "getCount"),
            @Mapping(target = "address", source = "element", qualifiedByName = "getAddress")
    })
    ProductCount toDto(Tuple element);

    @Named("getCount")
    default Long getCount(Tuple element) {
        return element.get(1, Long.class);
    }

    @Named("getAddress")
    default Address getAddress(Tuple element) {
        return element.get(0, Address.class);
    }


}
