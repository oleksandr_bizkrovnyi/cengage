package com.nixsolutions.dev.task.mapper;

import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.dto.AddressDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface AddressMapper {

    @Mappings({
            @Mapping(source = "addressId",target = "id"),
            @Mapping(source = "streetName",target = "street"),
            @Mapping(source = "cityName",target = "city")
    })
    Address toDomain(AddressDto addressDto);


    @Mappings({
            @Mapping(target = "addressId",source = "id"),
            @Mapping(source = "street",target = "streetName"),
            @Mapping(source = "city",target = "cityName"),
    })
    AddressDto toDto(Address address);


}
