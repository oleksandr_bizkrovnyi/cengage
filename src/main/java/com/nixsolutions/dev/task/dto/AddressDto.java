package com.nixsolutions.dev.task.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class AddressDto {

    @ApiModelProperty(notes = "Address UUID", required = true)
    private String addressId;

    @ApiModelProperty(notes = "City name", required = true)
    private String streetName;

    @ApiModelProperty(notes = "Street name", required = true)
    private String cityName;


}
