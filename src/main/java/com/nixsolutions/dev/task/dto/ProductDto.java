package com.nixsolutions.dev.task.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class ProductDto {

    @ApiModelProperty(notes = "Producer UUID", required = true)
    private String productIdentify;

    @ApiModelProperty(notes = "Product name", required = true)
    private String productName;

    @ApiModelProperty(notes = "Product price", required = true)
    private double cost;

}
