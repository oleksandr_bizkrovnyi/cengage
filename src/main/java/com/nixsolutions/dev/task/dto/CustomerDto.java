package com.nixsolutions.dev.task.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nixsolutions.dev.task.domain.Address;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class CustomerDto {
    @ApiModelProperty(notes = "Customer UUID", required = true)
    private String id;

    @ApiModelProperty(notes = "Customer name", required = true)
    private String name;

    @ApiModelProperty(notes = "Customer address", required = true)
    private Address address;
}
