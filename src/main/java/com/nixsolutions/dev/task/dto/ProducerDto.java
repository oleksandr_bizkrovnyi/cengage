package com.nixsolutions.dev.task.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.domain.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@XmlRootElement
@Data
public class ProducerDto {

    @ApiModelProperty(notes = "Producer UUID", required = true)
    private String id;

    @ApiModelProperty(notes = "Producer name", required = true)
    private String name;

    @ApiModelProperty(notes = "Producer address", required = true)
    private Address address;

    @ApiModelProperty(notes = "Producer product", required = true)
    private Product product;
}
