package com.nixsolutions.dev.task.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nixsolutions.dev.task.domain.Address;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class ProductCount implements Serializable {

    private long count;

    private Address address;

}

