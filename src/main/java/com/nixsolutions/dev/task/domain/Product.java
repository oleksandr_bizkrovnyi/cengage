package com.nixsolutions.dev.task.domain;


import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "PRODUCT")
@Data
public class Product implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @Column(name = "PRODUCT_ID")
    @NotBlank
    private String id;

    @Column(name = "NAME")
    @NotBlank
    private String name;

    @Column(name = "PRICE")
    @NotBlank
    private double price;

    @OneToMany(
    orphanRemoval =true,cascade = CascadeType.REMOVE)
    @NotBlank
    private List<Producer> producers;


}

