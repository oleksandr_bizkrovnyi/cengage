package com.nixsolutions.dev.task.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Entity
@Table(name = "ADDRESS")
@Data
public class Address implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @Column(name = "ADDRESS_ID")
    @NotBlank
    private String id;

    @NotBlank
    @Column(name = "CITY")
    private String city;

    @NotBlank
    @Column(name = "STREET")

    private String street;

}
