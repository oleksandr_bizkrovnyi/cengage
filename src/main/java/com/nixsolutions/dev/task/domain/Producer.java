package com.nixsolutions.dev.task.domain;



import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "PRODUCER")
@Data
public class Producer implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @NotBlank
    @Column(name = "PRODUCER_ID")
    private String id;

    @Column(name = "NAME")
    @NotBlank
    private String name;

    @ManyToOne(targetEntity = Address.class)
    @JoinColumn(name = "ADDRESS_ID")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Address address;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;


}
