package com.nixsolutions.dev.task.domain;


import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Entity
@Table(name = "CUSTOMER")
@Data
public class Customer implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @NotBlank
    @Column(name = "CUSTOMER_ID")
    private String id;

    @Column(name = "NAME")
    @NotBlank
    private String name;

    @ManyToOne(targetEntity = Address.class)
    @JoinColumn(name = "ADDRESS_ID")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Address address;


}
