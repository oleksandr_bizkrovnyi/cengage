package com.nixsolutions.dev.task.services;


import com.nixsolutions.dev.task.TaskApplication;
import com.nixsolutions.dev.task.dto.ProducerDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskApplication.class)
@Slf4j
public class ProducerServiceTest {


    @Autowired
    ProducerService producerService;

    private final String NAME = "prodMan";
    private final String ID = "325c5370-3046-4151-84e1-b789f7518974";
    private final String PRODUCT_ID = "FIX MAN";


    @Test
    public void findById() {
        ProducerDto found = producerService.findById(ID);
        assertNotNull(found);
        assertEquals(ID, found.getId());
    }
}
