package com.nixsolutions.dev.task.jmsListener;


import com.google.gson.Gson;
import com.nixsolutions.dev.task.domain.Address;
import com.nixsolutions.dev.task.domain.Customer;
import com.nixsolutions.dev.task.mapper.CustomerMapper;
import com.nixsolutions.dev.task.services.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JmsListenerTest {


    @Autowired
    private JmsTemplate template;

    @Autowired
    private CustomerService customerService;



    @Autowired
    private CustomerMapper mapper;

    private static Customer customer;


    @BeforeClass
    public static void setUp() {
        customer = new Customer();
        customer.setId(UUID.randomUUID().toString());
        customer.setName("Marmock");

        Address address = new Address();
        address.setId("325c5370-3046-4151-84e1-b789f75113123");
        address.setStreet("Custom Street");
        address.setCity("NY");
        customer.setAddress(address);
    }

    @Test
    public void testSendAndReceive() throws JMSException {
        customerService.save(mapper.toDto(customer));
        TextMessage receivedMessage = (TextMessage) template.receive("CustomerCreatedOutput");
        assertNotNull(receivedMessage);
        assertEquals(new Gson().toJson(customer), receivedMessage.getText());
    }
}
